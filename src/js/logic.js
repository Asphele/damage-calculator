$(document).ready(function() {
  // executes the code when the page is fully loaded and ready
  var inputs = document.querySelectorAll(".form-control");
  // this will make the inputs variable into an array so you can acces each input box in the html, for example inputs[0] etc...

  for (i = 0; i < inputs.length; i++) {
    inputs[i].classList.add("input" + i); // adds a extra class name for each input, easier to get the value and store it inside a variable with jquery
  }

  // ways to get the value
  //var a = inputs[0].value; // this is done with javascript
  //var b = $(".input0").val(); // this is used with Jquery

  // write your logic here
  $("#calculate-two").click(function() {
      //Jquery onclick function assigned
      var charLevel = parseInt(document.getElementById("charLevel2").value);
      var dmg = parseInt(document.getElementById("dmg2").value);
      var dmgBonus = parseInt(document.getElementById("dmgBonus2").value);
      var cDmg = parseInt(document.getElementById("cDmg2").value);
      var bDmg = parseInt(document.getElementById("bDmg2").value);
      var eDmg = parseInt(document.getElementById("eDmg2").value);
      var pen = parseInt(document.getElementById("pen2").value);
      var mobLevel = parseInt(document.getElementById("mobLevel2").value);
      var mobDef = 80;
      var dmgReduce = parseInt(document.getElementById("dmgReduce2").value);
      var fDmg = 0;

      if (isNaN(dmg)) {
          dmg = 0;
      }

      if (isNaN(dmgBonus)) {
          dmgBonus = 0;
      }
      if (isNaN(cDmg)) {
          cDmg = 100;
      }
      if (isNaN(bDmg)) {
          bDmg = 0;
      }
      if (isNaN(eDmg)) {
          eDmg = 0;
      }
      if (isNaN(pen)) {
          pen = 0;
      }
      if (isNaN(mobLevel)) {
          mobLevel = charLevel;
      }
      if (isNaN(dmgReduce)) {
          dmgReduce = 0;
      }

      if (document.getElementById("playerReduce2").checked) {
          pReduceVal = document.getElementById("PenShield2").value;
      } else {
          pReduceVal = 0;
      }

      var penShield = 0;
      if (document.getElementById("PenShield2").value == 0)
      {
          pen = 0;
      }
      else {
          penShield = document.getElementById("PenShield2").value;
      }
      if (isNaN(penShield))
      {
          penShield = 0;
      }

      var Awakening = 0;

      if (document.getElementById("Awaken2").checked)
      {
          Awakening = 1.05;
      }
      else {
          Awakening = 1;
      }

      var fDmg = (dmg * (cDmg / 100) * ((100 - dmgReduce + bDmg) / 100) * ((dmgBonus / 100) + 1) * ((eDmg / 100) + 1) * ((100 - penShield + pen) / 100) * ((100 - pReduceVal) / 100) * ((100 - mobDef) / 100) * (((charLevel - mobLevel) * 2 / 100) + 1) ) * Awakening;

      var value = Math.round(fDmg);

      document.getElementById("final-damage-two").innerHTML = value; // Getting element by id from the html and assigning a new value from the variable
      var x = document.getElementById("final-damage-two");
      if (x.textContent == "NaN") {
          x.textContent = "No Input Values";
          x.style.color = "#8B0000";
          x.style.fontWeight = "bold";
      }
      else {
          x.style.color = "#7CFC00";
          x.style.fontWeight = "bold";
      }
    check();
    checkInput(11, 20, "final-damage-two");
  });


  $("#calculate-one").click(function () {
      //Jquery onclick function assigned
      var charLevel = parseInt(document.getElementById("charLevel1").value);
      var dmg = parseInt(document.getElementById("dmg1").value);
      var dmgBonus = parseInt(document.getElementById("dmgBonus1").value);
      var cDmg = parseInt(document.getElementById("cDmg1").value);
      var bDmg = parseInt(document.getElementById("bDmg1").value);
      var eDmg = parseInt(document.getElementById("eDmg1").value);
      var pen = parseInt(document.getElementById("pen1").value);
      var mobLevel = parseInt(document.getElementById("mobLevel1").value);
      var mobDef = 80;
      var dmgReduce = parseInt(document.getElementById("dmgReduce1").value);
      var fDmg = 0;

      if (isNaN(dmg)) {
          dmg = 0;
      }

      if (isNaN(dmgBonus)) {
          dmgBonus = 0;
      }
      if (isNaN(cDmg)) {
          cDmg = 100;
      }
      if (isNaN(bDmg)) {
          bDmg = 0;
      }
      if (isNaN(eDmg)) {
          eDmg = 0;
      }
      if (isNaN(pen)) {
          pen = 0;
      }
      if (isNaN(mobLevel)) {
          mobLevel = charLevel;
      }
      if (isNaN(dmgReduce)) {
          dmgReduce = 0;
      }

      if (document.getElementById("playerReduce1").checked) {
          pReduceVal = document.getElementById("PenShield1").value;
      } else {
          pReduceVal = 0;
      }
      var penShield = 0;
      if (document.getElementById("PenShield1").value == 0) {
          pen = 0;
      }
      else {
          penShield = document.getElementById("PenShield1").value;
      }
      if (isNaN(penShield)) {
          penShield = 0;
      }
      
      var Awakening = 0;
      if(document.getElementById("Awaken1").checked)
      {
          Awakening = 1.05;
      }
      else
      {
          Awakening = 1;
      }

      var fDmg = (dmg * (cDmg / 100) * ((100 - dmgReduce + bDmg) / 100) * ((dmgBonus / 100) + 1) * ((eDmg / 100) + 1) * ((100 - penShield + pen) / 100) * ((100 - pReduceVal) / 100) * ((100 - mobDef) / 100) * (((charLevel - mobLevel) * 2 / 100) + 1) ) * Awakening;

      var value = Math.round(fDmg);


      document.getElementById("final-damage-one").innerHTML = value; // Getting element by id from the html and assigning a new value from the variable
      var x = document.getElementById("final-damage-one");
      if (x.textContent == "NaN")
      {
          x.textContent = "No Input Values";
          x.style.color = "#8B0000";
          x.style.fontWeight = "bold";
      }
      else
      {
          x.style.color = "#7CFC00";
          x.style.fontWeight = "bold";
      }
      check();
      checkInput(0, 10, "final-damage-one");
  });

  function check() {
    var shieldCheck = document.getElementById("penShield2"); // Getting element by id and assigning to to a variable

    if (shieldCheck.checked == true) {
      // checking if the checkbox has been checked
      console.log("It's checked");
    } else {
      return;
    }
  }

  function checkInput(start, end, target) {
    for (i = start; i < end; i++) {
      // making a for loop by the amount inputs that's been made, in the html
      if (inputs[i].value <= 0) {
        // checking the value input
        document.getElementById(target).innerHTML = // changing value from the final damage
          "All Inputs need to have a value";
        console.log(inputs[i].className + " Has no value been assigned");
      }
    }
  }
});
